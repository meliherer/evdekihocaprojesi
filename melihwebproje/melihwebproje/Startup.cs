﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(melihwebproje.Startup))]
namespace melihwebproje
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
